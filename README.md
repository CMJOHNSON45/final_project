573_env.yml
	This is a file that shows the channels and dependencies of the environment I used for my final project.

ERA5_1998_katabatic_slice.nc
	This is a sliced dataset that has ERA5 temperature, humidity, and wind speeds from December 1997 to December 1998 averaged on monthly pressure levels.

ERA5_1998_pres_slice.nc
	This is a sliced dataset that has ERA5 surface pressure from December 1997 to December 1998 averaged monthly.

final_project.ipynb
	This is my final project jupyter notebook where I analyzed the ERA5 data.
